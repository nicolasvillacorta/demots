// Objetos
var Avenger = /** @class */ (function () {
    function Avenger(nombre, equipo, nombreReal) {
        this.peleasGanadas = 0;
        this.nombre = nombre;
        this.equipo = equipo;
        this.nombreReal = nombreReal;
        console.log("se ejecuto el constructor");
    }
    return Avenger;
}());
var spiderman = new Avenger("SpiderMan", "Marvel", "Peter Parker");
console.log(spiderman);
console.log(spiderman.nombre);
console.log(spiderman.nombreReal);
