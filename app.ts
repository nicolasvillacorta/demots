// Objetos

class Avenger {

    nombre;
    equipo;
    nombreReal;
    puedePelear;
    peleasGanadas = 0;

    constructor(nombre, equipo, nombreReal){
        this.nombre = nombre;
        this.equipo = equipo;
        this.nombreReal = nombreReal;
        console.log("se ejecuto el constructor");
    }

}

let spiderman = new Avenger("SpiderMan", "Marvel", "Peter Parker");

console.log(spiderman);
console.log(spiderman.nombre);
console.log(spiderman.nombreReal);



